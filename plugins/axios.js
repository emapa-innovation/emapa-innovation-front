export default function ({ $axios, store, redirect }) {
    $axios.onRequest(config => {
        $nuxt.$loading.start()
    })

    $axios.onResponse(response => {
        $nuxt.$loading.finish()
    })

    $axios.onError(error => {
        $nuxt.$loading.finish()
        const code = parseInt(error.response && error.response.status)
        if (code === 401) {
            // if you ever get an unauthorized, logout the user
            store.dispatch('authLogout')
        } else {
            redirect('/error')
        }
    })
}