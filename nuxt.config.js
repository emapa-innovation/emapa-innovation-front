import colors from 'vuetify/es5/util/colors'
import webpack from 'webpack'
import es from 'vuetify/es5/locale/es'

var _baseURL = ''
if (process.env.FLAG_ENV === 'DEV') {
  _baseURL = process.env.BASE_URL_EI_DEV_WS
} else if (process.env.FLAG_ENV === 'TEST') {
  _baseURL = process.env.BASE_URL_EI_TEST_WS
} else if (process.env.FLAG_ENV === 'PROD') {
  _baseURL = process.env.BASE_URL_EI_PROD_WS
}

export default {
  // Disable server-side rendering (https://go.nuxtjs.dev/ssr-mode)
  ssr: false,

  // Target (https://go.nuxtjs.dev/config-target)
  target: 'static',

  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    titleTemplate: '%s - EMAPA INNOVATION',
    title: process.env.EMPRESA,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  env: {
    // BASE_URL_WS: process.env.BASE_URL_WS,
    EMPRESA: process.env.EMPRESA || null,
    ID_EMPRESA: process.env.ID_EMPRESA || null
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
  ],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    "~/plugins/vee-validate.js",
    '~/plugins/axios.js',
    '~/plugins/utils.js'
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
    '@nuxtjs/localforage'
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    '@nuxtjs/auth',
    'vue-sweetalert2/nuxt',
    '@nuxtjs/dotenv'
  ],

  router: {
    middleware: ['auth']
  },

  auth: {
    strategies: {
      local: {
        endpoints: {
          login: { url: 'seguridad/login', method: 'post', propertyName: 'data.token' },
          user: false,
          logout: false
        },
        tokenType: false,
        autoFetchUser: false
      }
    }
  },

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {
    // baseURL: process.env.BASE_URL_WS
    baseURL: _baseURL
  },

  // Vuetify module configuration (https://go.nuxtjs.dev/config-vuetify)
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    },
    lang: {
      locales: { es },
      current: 'es',
    }
  },

  loading: '~/components/LoadingBar.vue',

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
    transpile: ["vee-validate/dist/rules"],
    plugins: [
      new webpack.ProvidePlugin({
        // global modules
        $: 'jquery',
        _: 'lodash'
      })
    ]
  }
}
