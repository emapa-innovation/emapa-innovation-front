export const actions = {
    authLogout ({ commit }) {
        this.$auth.logout()
    }
}

export const state = () => ({
    module: null,
})
     
export const mutations = {
    CHANGE_MODULE_SELECTED(state, module) {
        state.module = module
    }
}