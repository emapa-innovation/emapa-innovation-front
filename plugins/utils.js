const utils = {
    ping() {
        console.log('Ping')
    },
    generarCabeceraDtt(items) {
        return _.map(_.keys(_.head(items)), function(value) {
          return { text: value, value: value }
        })
    }
}

export default (context, inject) => {
    inject('utils', utils)
}
