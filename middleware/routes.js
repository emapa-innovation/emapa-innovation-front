import someDeep from 'deepdash/someDeep';

export default function ({ route, app, redirect }) {
  const menuOptions = app.$auth.$storage.getLocalStorage('ei_menu_options')

  const routeFound = someDeep(menuOptions, function(value) {
    return value === route.fullPath;
  })

  if ( !routeFound ) {
    return redirect('/')
  }
}
